#pragma once
#include <iostream>
#include <string>
#include "Position.h"

class Board
{
public:
	Board();
	~Board();
	bool setData(unsigned int index, std::string piece);
	void GetAITurn(std::string aiPiece);
	int checkWin();
	void DisplayBoard();
	const int MAX_SPACES = 9;

private:
	std::array<std::string, 9> data = { "~", "~", "~",
									    "~", "~", "~",
									    "~", "~", "~" };
	std::string board;
	std::string key;
	Position* currentPosition;
};

