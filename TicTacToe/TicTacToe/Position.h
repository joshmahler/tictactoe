#pragma once
#include <array>
#include <tuple>
#include <math.h>

class Position
{
public:
	Position();
	~Position();
	void SetData(unsigned int index, int piece);
	int getRandomInput();
	int getOptimalMove();
	int checkWin();

private:
	std::tuple<int, int> getPieces();
	void EmptyHelper(int index, int& emptyCount, int& emptyIndex, int& savedPiece, int& winner,
		int& counter, bool& didSetPiece, bool& didBreak, bool careAboutPiece);
	std::tuple<int, int> onlyEmptyInRow(int index, bool careAboutPiece);
	std::tuple<int, int> onlyEmptyInColumn(int index, bool careAboutPiece);
	std::tuple<int, int> onlyEmptyInDiagonal(int startingCornerIndex, bool careAboutPiece);
	int getOptimalDiagonal();
	int getAdjacentSlotToMiddleSide(int index);
	bool getIsSameRow(int index0, int index1);
	bool getIsSameColumn(int index0, int index1);
	int getOTrap();
	int checkCanWinOrBlock(bool onlyCheckWin);

	const int MIDDLE = 4;
	std::array<int, 9> position;
	std::array<int, 4> corners;
	std::array<int, 4> middleSides;
	int dim;
	int usedSlots;
	int firstX, firstO, secondX;
	bool isXTrapSetup, isOTrapSetup;
};

