#pragma once
#include <ctime>
#include "Board.h"

class Game
{
public:
	Game();
	~Game();
	void Init();
	void Reset(bool playAgain);
	void Menu();
	void Update();

private:
	void GetInput();
	void DisplayWinner(int winner);
	void DisplayWinrate();

	Board* board;
	bool isPlayerTurn;
	std::string chosenPiece;
	std::string aiPiece;
	int xWin, oWin;
};

