#include "Game.h"

Game::Game()
{
	Init();
	xWin = 0;
	oWin = 0;
}

Game::~Game()
{
	Reset(false);
}

void Game::Init()
{
	board = new Board();
	std::srand((unsigned int)std::time(nullptr));
	Menu();
	Update();
}

void Game::Reset(bool playAgain)
{
	delete board;
	if (playAgain)
	{
		Init();
	}
}

void Game::Menu()
{
	char piece;
	do
	{
		system("cls");
		std::cout << "Welcome to Tic Tac Toe!\n\n";
		std::cout << "Do you want to be X or O? ";

		std::string input;
		std::getline(std::cin, input);
		piece = std::toupper(input[0]);

	} while (piece != 'X' && piece != 'O');

	chosenPiece = piece;
	if (piece == 'X')
	{
		aiPiece = "O";
		isPlayerTurn = true;
	}
	else
	{
		aiPiece = "X";
		isPlayerTurn = false;
	}
}

void Game::Update()
{
	int win = 0;
	do
	{
		DisplayWinrate();
		board->DisplayBoard();

		if (isPlayerTurn)
		{
			GetInput();
			isPlayerTurn = false;
		}
		else
		{
			board->GetAITurn(aiPiece);
			isPlayerTurn = true;
		}

		win = board->checkWin();

	} while (win == 0);

	DisplayWinner(win);
}

void Game::GetInput()
{
	std::string input;
	int num;
	bool retry;
	bool didCatch;
	do
	{
		retry = false;
		didCatch = false;
		std::cout << "Your move: ";
		std::cin >> input;
		
		try 
		{
			num = std::stoi(input);
		}
		catch (std::invalid_argument const& e)
		{
			std::cout << "Bad input: std::invalid_argument thrown\n";
			retry = true;
			didCatch = true;
		}
		catch (std::out_of_range const& e)
		{
			std::cout << "Integer overflow: std::out_of_range thrown\n";
			retry = true;
			didCatch = true;
		}

		if (!didCatch && (num < 0 || num >= board->MAX_SPACES))
		{
			retry = true;
		}

	} while (retry);

	bool newToBoard = board->setData(num, chosenPiece);
	if (!newToBoard)
	{
		std::cout << "Slot is full\n";
		GetInput();
	}
}

void Game::DisplayWinrate()
{
	system("cls");
	std::string winrate;
	winrate += "X: " + std::to_string(xWin) + "\n";
	winrate += "O: " + std::to_string(oWin) + "\n\n\n";
	std::cout << winrate;
}

void Game::DisplayWinner(int winner)
{
	DisplayWinrate();
	board->DisplayBoard();
	std::string winMsg;
	if (winner == 1)
	{
		winMsg = "X wins!\n";
		++xWin;
	}
	else if(winner == -1)
	{
		winMsg = "O wins!\n";
		++oWin;
	}
	else
	{
		winMsg = "It's a draw!\n";
	}

	std::cout << winMsg;
	system("pause");
	Reset(true);
}