#include "Board.h"

Board::Board()
{
	currentPosition = new Position();
}

Board::~Board()
{
	delete currentPosition;
}

bool Board::setData(unsigned int index, std::string piece)
{
	if (index < data.size())
	{
		if (data[index] == "X" || data[index] == "O")
			return false;

		if (piece == "X")
			currentPosition->SetData(index, 1);
		else
			currentPosition->SetData(index, -1);

		data[index] = piece;
		return true;
	}
	return false;
}

void Board::GetAITurn(std::string aiPiece)
{
	int index = currentPosition->getOptimalMove();
	if (index == -1)
	{
		bool newToBoard;
		do
		{
			index = currentPosition->getRandomInput();
			newToBoard = setData(index, aiPiece);
		} while (!newToBoard);
	}
	else
	{
		bool newToBoard = setData(index, aiPiece);
		if (!newToBoard)
			GetAITurn(aiPiece);
	}
}

/*
Will return:
0 if no win
1 if X win
-1 if O win
*/
int Board::checkWin()
{
	return currentPosition->checkWin();
}

void Board::DisplayBoard()
{
	key = "";
	board = "";
	unsigned int col = 0;
	for (unsigned int i = 0; i < data.size(); ++i)
	{
		key += std::to_string(i) + " ";
		board += data[i] + " ";

		if (++col >= data.size() / 3)
		{
			col = 0;
			key += "\n";
			board += "\n";
		}
	}

	std::cout << key << std::endl;
	std::cout << board << std::endl;
}