#include "Position.h"
#include <vector>

Position::Position()
{
	position.fill(0);
	dim = (int)sqrt(position.size());
	corners = { 0, 2, 6, 8 };
	middleSides = { 1, 3, 5, 7 };
	usedSlots = 0;
	firstX = -1;
	firstO = -1;
	secondX = -1;
	isXTrapSetup = false;
	isOTrapSetup = false;
}

Position::~Position()
{

}

void Position::SetData(unsigned int index, int piece)
{
	if (index < position.size())
	{
		if (position[index] != 0)
			return;

		position[index] = piece;
		++usedSlots;
	}
}

std::tuple<int, int> Position::getPieces()
{
	int myPiece, opponentPiece;

	//even number of slots used means it's X's turn
	if (usedSlots % 2 == 0)
	{
		myPiece = 1;
		opponentPiece = -1;
	}
	else
	{
		myPiece = -1;
		opponentPiece = 1;
	}

	return std::make_tuple(myPiece, opponentPiece);
}

//If careAboutWin is true, myPiece must be set
void Position::EmptyHelper(int index, int& emptyCount, int& emptyIndex, int& savedPiece, int& winner,
	int& counter, bool& didSetPiece, bool& didBreak, bool careAboutPiece)
{
	++counter;
	//A position being 0 means its empty
	if (position[index] == 0)
	{
		if (++emptyCount > 1)
		{
			emptyIndex = -1;
			return;
		}
		emptyIndex = index;

		//Edge case where middle slot(s) are empty and sides are not
		if (1 < counter && counter < dim)
			return;
	}
	//If position isn't 0, save the piece active in slot for later checks
	if (careAboutPiece && position[index] != 0)
	{
		if (didSetPiece)
		{
			if (position[index] != savedPiece)
			{
				//If we don't care about win, and the savedPiece isn't the same
				//as the current one we're checking, reset emptyIndex bc slot is invalid
				emptyIndex = -1;
				winner = 0;
				didBreak = true;
				return;
			}
		}
		else
		{
			//If didSetPiece is false, set the piece
			savedPiece = position[index];
			winner = savedPiece;
			didSetPiece = true;
		}
	}
}

/*
This is useful for checking where not to go if careAboutPiece is false,
and also useful for checking where I can win if careAboutPiece is true.
Returns -1 if invalid (no/multiple empties in this row)
else returns index of only empty in this row
*/
std::tuple<int, int> Position::onlyEmptyInRow(int index, bool careAboutPiece)
{
	int row = index / dim;
	int startingIndex = row * dim;

	int emptyCount = 0, emptyIndex = -1, savedPiece = 0, winner = 0, counter = 0;
	bool didSetPiece = false, didBreak = false;

	//Since indices go up as a book is read, only need to check 
	//startingIndex -> startingIndex + dim
	for (int i = startingIndex; i < startingIndex + dim; ++i)
	{
		EmptyHelper(i, emptyCount, emptyIndex, savedPiece, winner, counter, didSetPiece, didBreak, careAboutPiece);
		if (didBreak)
			break;
	}

	return std::make_tuple(emptyIndex, winner);
}

/*
This is useful for checking where not to go if careAboutPiece is false,
and also useful for checking where I can win if careAboutPiece is true.
Returns -1 if invalid (no/multiple empties in this col)
else returns index of only empty in this col
*/
std::tuple<int, int> Position::onlyEmptyInColumn(int index, bool careAboutPiece)
{
	int col = index % dim;

	int emptyCount = 0, emptyIndex = -1, savedPiece = 0, winner = 0, counter = 0;
	bool didSetPiece = false, didBreak = false;

	//Since indices go up as a book is read:
	//increment i by dim to get the column, starting at the column
	for (unsigned int i = col; i < position.size(); i += dim)
	{
		EmptyHelper(i, emptyCount, emptyIndex, savedPiece, winner, counter, didSetPiece, didBreak, careAboutPiece);
		if (didBreak)
			break;
	}

	return std::make_tuple(emptyIndex, winner);
}

std::tuple<int, int> Position::onlyEmptyInDiagonal(int startingCornerIndex, bool careAboutPiece)
{
	//"MainDiagonal" is the topleft-bottomright diagonal
	//If false, it is the topright-bottomleft diagonal
	bool mainDiagonal = false;
	if (startingCornerIndex == 0 || startingCornerIndex == position.size() - 1)
		mainDiagonal = true;

	int emptyCount = 0, emptyIndex = -1, savedPiece = 0, winner = 0, counter = 0;
	bool didSetPiece = false, didBreak = false;

	if (mainDiagonal)
	{
		//Since indices go up as a book is read:
		//increment i by dim to get the column, starting at the column
		for (unsigned int i = 0; i < position.size(); i += (dim + 1))
		{
			EmptyHelper(i, emptyCount, emptyIndex, savedPiece, winner, counter, didSetPiece, didBreak, careAboutPiece);
			if (didBreak)
				break;
		}
	}
	else
	{
		//Since indices go up as a book is read:
		//increment i by dim to get the column, starting at the column
		for (unsigned int i = dim - 1; i < position.size() - (dim - 1); i += (dim - 1))
		{
			EmptyHelper(i, emptyCount, emptyIndex, savedPiece, winner, counter, didSetPiece, didBreak, careAboutPiece);
			if (didBreak)
				break;
		}
	}

	return std::make_tuple(emptyIndex, winner);
}

/*
This is a helper function for the second move of X.
*/
int Position::getOptimalDiagonal()
{
	int optimalIndex = -1;
	std::vector<int> optimalCorner;
	int notOptimal = (position.size() - 1) - firstX;

	//Add corners to consider
	for (unsigned int i = 0; i < corners.size(); ++i)
	{
		if (corners[i] != firstX &&
			corners[i] != notOptimal &&
			corners[i] != firstO)
		{
			optimalCorner.push_back(corners[i]);
		}
	}

	if (optimalCorner.size() == 1)
	{
		//This will happen if O went to the corner, 
		//but not the corner opposite my first move
		//Skipping function calls is cool.
		optimalIndex = optimalCorner[0];
	}
	else
	{
		//Check if O selected adjacent slot to first X
		int emptyRowIndex, winnerRow;
		std::tie(emptyRowIndex, winnerRow) = onlyEmptyInRow(firstO, false);
		int emptyColIndex, winnerCol;
		std::tie(emptyColIndex, winnerCol) = onlyEmptyInColumn(firstO, false);

		for (unsigned int i = 0; i < optimalCorner.size(); ++i)
		{
			if (emptyRowIndex != -1)
			{
				if (optimalCorner[i] == emptyRowIndex)
				{
					//This is the same row as the O went, not optimal
					optimalCorner.erase(optimalCorner.begin() + i);
					--i;
					continue;
				}
			}
			if (emptyColIndex != -1)
			{
				if (optimalCorner[i] == emptyColIndex)
				{
					//This is the same column as the O went, not optimal
					optimalCorner.erase(optimalCorner.begin() + i);
					--i;
					continue;
				}
			}
		}

		if (optimalCorner.size() == 1)
		{
			//There's only one option left! Choose this one.
			optimalIndex = optimalCorner[0];
		}
		else
		{
			//There's two options left! Random between them.
			int random = rand() % optimalCorner.size();
			optimalIndex = optimalCorner[random];
		}
	}

	return optimalIndex;
}

int Position::getAdjacentSlotToMiddleSide(int index)
{
	std::vector<int> validIndices;

	//Test which middle side it is
	bool sameRow = getIsSameRow(index, index - 1);
	bool sameRow1 = getIsSameRow(index, index + 1);

	if (sameRow && sameRow1)
	{
		//If they are the same row, X went top or bottom middle
		//optimalIndex is either corner in same row
		for (unsigned int i = 0; i < corners.size(); ++i)
		{
			sameRow = getIsSameRow(index, corners[i]);
			if (sameRow)
			{
				validIndices.push_back(corners[i]);
			}
		}
	}
	else
	{
		//If they are not in the same row, X went left or right middle
		//optimalIndex is either corner in same column
		int xCol = index % dim;

		for (unsigned int i = 0; i < corners.size(); ++i)
		{
			int oCol = corners[i] % dim;
			if (xCol == oCol)
			{
				validIndices.push_back(corners[i]);
			}
		}
	}

	int random = std::rand() % validIndices.size();
	return validIndices[random];
}

bool Position::getIsSameRow(int index0, int index1)
{
	int row0 = index0 / dim;
	int row1 = index1 / dim;
	return row0 == row1;
}

bool Position::getIsSameColumn(int index0, int index1)
{
	int col0 = index0 % dim;
	int col1 = index1 % dim;
	return col0 == col1;
}

//Returns index of optimal index, -1 if not possible to trap
int Position::getOTrap()
{
	bool sameRow = getIsSameRow(firstO, secondX);
	bool sameCol = getIsSameColumn(firstO, secondX);

	//Make sure it's not in the same row or column as my first move
	if (!sameRow && !sameCol)
	{
		//Go to the corner in the same row/column as my first move
		//But not the one blocked by X's first move
		for (unsigned int i = 0; i < corners.size(); ++i)
		{
			if (position[corners[i]] == 0)
			{
				//Remove any blocked by X first move
				sameRow = getIsSameRow(firstX, corners[i]);
				sameCol = getIsSameColumn(firstX, corners[i]);
				if (!sameRow && !sameCol)
				{
					//Remove any not in same row/col as my first move
					sameRow = getIsSameRow(firstO, corners[i]);
					sameCol = getIsSameColumn(firstO, corners[i]);
					if (sameRow || sameCol)
					{
						return corners[i];
					}
				}
			}
		}
	}
	else
	{
		sameRow = getIsSameRow(firstX, secondX);
		sameCol = getIsSameColumn(firstX, secondX);
		if (!sameRow && !sameCol)
		{
			//secondX went to slot on opposite side of board than firstO
			return MIDDLE;
		}
		else
		{
			//secondX went to the slot adjacent to X's first move in corner
			for (unsigned int i = 0; i < middleSides.size(); ++i)
			{
				sameRow = getIsSameRow(firstO, middleSides[i]);
				sameCol = getIsSameColumn(firstO, middleSides[i]);
				if (sameRow || sameCol)
				{
					return middleSides[i];
				}
			}
		}
	}

	//If it is in either the row or column, the trap cannot work
	return -1;
}

int Position::getRandomInput()
{
	int index;
	bool newToBoard;
	do
	{
		index = std::rand() % position.size();

		newToBoard = true;
		if (position[index] != 0)
			newToBoard = false;

	} while (!newToBoard);

	return index;
}

/*
Returns -1 if move doesn't matter
*/
int Position::getOptimalMove()
{
	int optimalIndex = -1;
	if (usedSlots > 2)
	{
		//int winOrBlockIndex = checkCanWinOrBlock(false);
		optimalIndex = checkCanWinOrBlock(true);
		//Problem here!
		//I want to win whenever possible, but can't!

		if (/*winOrBlockIndex == optimalIndex && */optimalIndex != -1)
			return optimalIndex;
	}

	int random;

	switch (usedSlots)
	{
	case 0:
		/*
			I am X.
			Any corner.
		*/
		random = rand() % corners.size();
		optimalIndex = corners[random];
		firstX = optimalIndex;
		break;

	case 1:
		/*
		I am O.
		If X decides to start on the middle side
		Go to an adjecent slot

		If X decides to start on any corner
		O should go to middle

		If X decides to start on the middle
		O should go to any corner
		*/
		//This is the only issue, many things to check
	{
		bool didBreak = false;
		if (position[MIDDLE] != 0)
		{
			//X went middle
			firstX = MIDDLE;
			random = rand() % corners.size();
			optimalIndex = corners[random];
			firstO = optimalIndex;
			break;
		}
		for (unsigned int i = 0; i < corners.size(); ++i)
		{
			if (position[corners[i]] != 0)
			{
				//X went corner
				firstX = corners[i];
				optimalIndex = MIDDLE;
				firstO = optimalIndex;
				didBreak = true;
				break;
			}
		}
		if (!didBreak)
		{
			for (unsigned int i = 0; i < middleSides.size(); ++i)
			{
				if (position[middleSides[i]] != 0)
				{
					//X went middle side
					firstX = middleSides[i];
					optimalIndex = getAdjacentSlotToMiddleSide(firstX);
					isOTrapSetup = true;
					firstO = optimalIndex;
					break;
				}
			}
		}
	}
	break;

	case 2:
		/*
		I am X.
		I went to a corner on the first move

		If O did NOT go to middle, I can win by trap.
		Do NOT go to:

		index = mirror(8) - myFirstMove
		0 -> 8
		8 -> 0
		2 -> 6
		6 -> 2
		

		If O is next to the first X:
		Do NOT go to:
		same row/column corner as O

		There will be 1 or 2 remaining corners, 
		if 1, that's the optimalIndex
		if 2, random between them

		Else if: O is middle
		This is a drawn position assuming both players play optimally.
		To force a draw, go next to (same row/column) as first X move.
		*/
		for (unsigned int i = 0; i < position.size(); ++i)
		{
			if (i != firstX && position[i] != 0)
			{
				firstO = i;
				break;
			}
		}

		if (position[MIDDLE] != 0)
		{
			//O went middle
			random = std::rand() % middleSides.size();
			optimalIndex = middleSides[random];
		}
		else
		{
			//O did not go middle, win by trap possible
			isXTrapSetup = true;
			optimalIndex = getOptimalDiagonal();
		}
		break;

	case 3:
		/*
		I am O. 
		This test is if X went middle side first move.
		I went next to X, either corner.
		then X went middle side second move.
		If we didn't form an L shape with our moves, I can trap X.
		I must go to the corner in the same row/column on the opposite side
		of the board from X's first move.
		Note: This trap can be forced if X goes to the mirror of X's first move.
		Therefore, it will be on my third move (case 5).
		*/
		if (isOTrapSetup)
		{
			for (unsigned int i = 0; i < position.size(); ++i)
			{
				//Find the second X index
				if (position[i] != 0 &&
					i != firstX &&
					i != firstO)
				{
					secondX = i;
				}
			}

			//If it's found, it won't be -1
			if (secondX != -1)
			{
				int secondO = getOTrap();
				if (secondO == -1)
				{
					isOTrapSetup = false;
				}
				optimalIndex = secondO;
			}
		}
		else if(firstX == MIDDLE)
		{
			/*
			If X goes middle.
			I go corner.
			There's an edge case where X goes to the blocked corner and can win if I mess up.
			Force me to go to a corner not taken yet to avoid this.
			*/
			std::vector<int> validIndices;
			for (unsigned int i = 0; i < corners.size(); ++i)
			{
				if (position[corners[i]] == 0)
				{
					validIndices.push_back(corners[i]);
				}
			}
			if (validIndices.size() > 0)
			{
				random = std::rand() % validIndices.size();
				optimalIndex = validIndices[random];
			}
		}
		break;
			   
	case 4:
		/*
		I am X.
		I went to a corner on the first move.
		This case is for if I set up a trap on the second move.
		E.g. O did not go middle on first move
		This part is simple because I just want to go to the opposite corner
		as my first move.
		Check for edge case where O went to the opposite corner on first move
		*/
		if (isXTrapSetup)
		{
			optimalIndex = (position.size() - 1) - firstX;
			if (optimalIndex == firstO)
			{
				for (unsigned int i = 0; i < corners.size(); ++i)
				{
					if (position[corners[i]] == 0)
					{
						optimalIndex = corners[i];
						break;
					}
				}
			}
		}
		break;

	case 5:
		/*
		I am O.
		See case 3 for forced O trap.
		X goes middle side first.
		I go adjacent to X's move (corner).
		X goes to the middle side opposite X's first move.
		I am forced block in the middle.
		X is forced to block the diagonal.
		I can trap by going to the corner in the same row/column as my first move,
		not the one in the same row/column as X's first move.
		*/
		int thirdO = getOTrap();
		if (thirdO != -1 && position[thirdO] == 0)
		{
			optimalIndex = thirdO;
		}
		break;
	}

	return optimalIndex;
}

/*
empty slot: 0

myPiece is either: 
X: 1
O: -1

This is called when it's "my" turn
*/
int Position::checkCanWinOrBlock(bool onlyCheckWin)
{
	int myPiece, opponentPiece;
	std::tie(myPiece, opponentPiece) = getPieces();

	std::vector<std::tuple<int, int>> validIndices;

	//Check if there is a "only empty" in rows/cols/diagonals

	//Row
	for (unsigned int i = 0; i < position.size(); i += dim)
	{
		int emptyRowIndex, winner;
		std::tie(emptyRowIndex, winner) = onlyEmptyInRow(i, onlyCheckWin);
		if (emptyRowIndex != -1)
		{
			validIndices.push_back(std::make_tuple(emptyRowIndex, winner));
		}
	}

	//Col
	for (int i = 0; i < dim; ++i)
	{
		int emptyColIndex, winner;
		std::tie(emptyColIndex, winner) = onlyEmptyInColumn(i, onlyCheckWin);
		
		if (emptyColIndex != -1)
		{
			validIndices.push_back(std::make_tuple(emptyColIndex, winner));
		}
	}

	//Main diagonal
	int emptyMainDiagonalIndex, winner;
	std::tie(emptyMainDiagonalIndex, winner) = onlyEmptyInDiagonal(0, onlyCheckWin);
	if (emptyMainDiagonalIndex != -1)
	{
		validIndices.push_back(std::make_tuple(emptyMainDiagonalIndex, winner));
	}

	//Other diagonal
	int emptyOtherDiagonalIndex;
	std::tie(emptyOtherDiagonalIndex, winner) = onlyEmptyInDiagonal(dim - 1, onlyCheckWin);
	if (emptyOtherDiagonalIndex != -1)
	{
		validIndices.push_back(std::make_tuple(emptyOtherDiagonalIndex, winner));
	}

	for (unsigned int i = 0; i < validIndices.size(); ++i)
	{
		if (std::get<1>(validIndices[i]) == myPiece)
			return std::get<0>(validIndices[i]);
	}
	if (validIndices.size() > 0)
	{
		int random = std::rand() % validIndices.size();
		return std::get<0>(validIndices[random]);
	}

	return -1;
}

/*
Will return:
0 if no win
1 if X win
-1 if O win
2 if tie
*/
int Position::checkWin()
{
	/*
	Possible wins: 
	Row: 012, 345, 678
	Column: 036, 147, 258
	Diagonal: 048, 246
	*/

	//Check win in rows
	for (unsigned int i = 0; i < position.size(); i += dim)
	{
		bool didWin = true;
		int slot;
		for (int j = 0; j < dim - 1; ++j)
		{
			slot = i + j;
			if (position[slot] == 0 ||
				position[slot] != position[slot + 1])
			{
				didWin = false;
				break;
			}
		}
		if (didWin)
		{
			return position[slot];
		}
	}

	//Check win in columns
	for (int i = 0; i < dim; ++i)
	{
		bool didWin = true;
		int slot;
		for (unsigned int j = 0; j < position.size() - dim; j += dim)
		{
			slot = i + j;
			if (position[slot] == 0 ||
				position[slot] != position[slot + dim])
			{
				didWin = false;
				break;
			}
		}
		if (didWin)
		{
			return position[slot];
		}
	}

	//Check win in diagonals
	bool didWin = true;
	int startingCorner = 0;

	//Top left to bottom right diagonal
	for (unsigned int current = 0; current < position.size(); current += (dim + 1))
	{
		if (position[current] == 0 ||
			position[startingCorner] != position[current])
		{
			didWin = false;
			break;
		}
	}
	if (didWin)
	{
		return position[startingCorner];
	}

	didWin = true;
	startingCorner += dim - 1;
	//Top right to bottom left diagonal
	for (unsigned int current = dim - 1; current < position.size() - (dim - 1); current += (dim - 1))
	{
		if (position[current] == 0 ||
			position[startingCorner] != position[current])
		{
			didWin = false;
			break;
		}
	}
	if (didWin)
	{
		return position[startingCorner];
	}

	if (usedSlots >= position.size())
	{
		//Returning 2 means its a tie
		return 2;
	}

	//If it gets here, nobody wins yet
	return 0;
}